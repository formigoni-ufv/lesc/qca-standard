#include "genericqcacell.h"

GenericQcaCell::GenericQcaCell(const GenericQcaCellType& genericQcaCellType) : genericQcaCellType(genericQcaCellType) {}

void GenericQcaCell::set_type(const GenericQcaCellType& genericQcaCellType) {
	this->genericQcaCellType = genericQcaCellType;
}

const GenericQcaCellType GenericQcaCell::get_type() const {
	return this->genericQcaCellType;
}

const unsigned GenericQcaCell::get_code() const {
	return (unsigned) this->genericQcaCellType;
}
