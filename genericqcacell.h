#pragma once
#include <string>

enum class GenericQcaCellType : unsigned{
	UNDEFINED = 0,
	WIRE      = 1,
	FIXED0    = 3,
	FIXED1    = 4,
	INPUT     = 5,
	VERTICAL  = 6,
	CROSSOVER = 7,
	OUTPUT    = 8
};

class GenericQcaCell{ //Generic QCA Cell
	public:
		GenericQcaCell() = default;
		GenericQcaCell(const GenericQcaCellType& genericQcaCellType);
	private:
		GenericQcaCellType genericQcaCellType;
	public:
		void set_type(const GenericQcaCellType& genericQcaCellType);
		const GenericQcaCellType get_type() const;
		const unsigned get_code() const;
};
