#include "genericqcacellgrid.h"

using std::ostream;
using std::endl;
using std::vector;

void GenericQcaCellGrid::add(const GenericQcaCell& cell, const unsigned& x, const unsigned& y){
	GenericQcaCell ucell(GenericQcaCellType::UNDEFINED);

	while(this->grid.size() <= y) { grid.push_back(vector<GenericQcaCell>()); }
	while(this->grid[y].size() <= x) { grid[y].push_back(ucell); }

	this->grid[y][x] = cell;
}

void GenericQcaCellGrid::print(ostream& output) const  {
	unsigned currOffset=0;
	unsigned maxOffset=0;

	if(output.fail())
		throw std::invalid_argument("Erro ao acessar o stream de saída");

	for(auto& line : this->grid){
		for(auto& column : line){
			if(&column != &line[line.size()-1]){
				if(column.get_type() != GenericQcaCellType::UNDEFINED){
					output << column.get_code() << " ";
				}else{
					output << "  ";
				}
			}else{
				output << column.get_code();
			}
		}
		output << endl;
	}

	if(output.fail())
		throw std::runtime_error("Erro ao escrever no stream de saida");
}

