#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include "genericqcacell.h"
#include "genericqcacellgrid.h"

using std::ifstream;
using std::string;
using std::cerr;
using std::cout;
using std::endl;
using std::ofstream;

void decodeQcaDesinerFile(GenericQcaCellGrid& grid, const std::string& filename);

int main(int argc, char *argv[]){
	GenericQcaCell gcell(GenericQcaCellType::WIRE);
	GenericQcaCellGrid grid;

	if(argc >= 4){
		if( !strcmp(argv[1], "-o") && !strcmp(argv[3], "-f") ){
			try{
				decodeQcaDesinerFile(grid, (std::string) argv[4]);
			}catch(const std::invalid_argument& e){
				cerr << e.what() << endl;
			}catch(const std::runtime_error& e){
				cerr << e.what() << endl;
			}

			try{
				ofstream outFile(argv[2]);
				grid.print(outFile);
			}catch(const std::invalid_argument& e){
				cerr << e.what() << endl;
			}catch(const std::runtime_error& e){
				cerr << e.what() << endl;
			}

		}else if( !strcmp(argv[3], "-o") && !strcmp(argv[1], "-f") ){
			try{
				decodeQcaDesinerFile(grid, (std::string) argv[2]);
			}catch(const std::invalid_argument& e){
				cout << e.what() << endl;
			}catch(const std::runtime_error& e){
				cout << e.what() << endl;
			}

			try{
				ofstream outFile(argv[4]);
				grid.print(outFile);
			}catch(const std::invalid_argument& e){
				cerr << e.what() << endl;
			}catch(const std::runtime_error& e){
				cerr << e.what() << endl;
			}
		}
	}
	return -1;
}

void decodeQcaDesinerFile(GenericQcaCellGrid& grid, const std::string& filename){
	GenericQcaCell* gCell;
	unsigned x, y;
	char* endPtr;
	char c;
	string line;
	ifstream srcFile(filename);

	if(srcFile.fail())
		throw std::invalid_argument("Erro ao abrir o arquivo de entrada: " + filename);

	while(srcFile >> line){
		if(line == "[TYPE:QCADCell]"){
			gCell = new GenericQcaCell();
			srcFile >> line >> line;
			x = std::strtof(line.substr(2).c_str(), &endPtr); srcFile >> line;
			y = std::strtof(line.substr(2).c_str(), &endPtr);
			x = x/20;
			y = y/20;
			//std::cout << "x: " << x << endl << "y: " << y << endl;
			for(int i=0; i<16; i++) srcFile >> line;
			line = line.substr(14);
			//std::cout << line << endl;
			if(line == "QCAD_CELL_NORMAL"){
				gCell->set_type(GenericQcaCellType::WIRE);
			}else if(line == "QCAD_CELL_INPUT"){
				gCell->set_type(GenericQcaCellType::INPUT);
			}else if(line == "QCAD_CELL_OUTPUT"){
				gCell->set_type(GenericQcaCellType::OUTPUT);
			}
			grid.add(*gCell, x, y);
			delete(gCell);
		}
	}

	if(!srcFile.eof())
		throw std::runtime_error("Erro durante a leitura do arquivo de entrada");
}
